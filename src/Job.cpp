#include "Job.hpp"

Job::Job(std::string startsAt, std::string endsAt, std::string jobTitle_, std::string userFirstName_, std::string userLastName_)
    :startDate(startsAt), endDate(endsAt)
{
    jobTitle = jobTitle_;
    userFirstName = userFirstName_;
    userLastName = userLastName_;
}

void Job::print()
{
    std::cout << startDate.toString() << " - " << endDate.toString() << " " << jobTitle << " by " << userFirstName << ' ' << userLastName;
}

bool Job::operator < (const Job& job) const
{
    if(startDate != job.startDate)
        return startDate < job.startDate;
    return endDate < job.endDate;
}
