#include "Skill.hpp"

Skill::Skill(std::string name_, float rate_)
{
    name = name_;
    rate = rate_;
}

void Skill::endorseByUser(std::string endorserId, int endorserCount)
{
    std::vector<std::string>::iterator endorserUserIter = std::find(endorsersId.begin(), endorsersId.end(), endorserId);
    if(endorserUserIter != endorsersId.end())
        return;
    rate += sqrt(endorserCount + 1);
    endorsersId.push_back(endorserId);
}
