#include "Date.hpp"

Date::Date(std::string date)
{
    if(date == NOW)
    {
        day = INF;
        month = INF;
        year = INF;
        return;
    }
    int slashPos = date.find('/');
    day = atoi(date.substr(0, slashPos).c_str());
    if(day < 1 || day > 31)
        day = 1;
    date = date.substr(slashPos + 1);
    slashPos = date.find('/');
    month = atoi(date.substr(0, slashPos).c_str());
    if(month < 1 || month > 12)
        month = 1;
    date = date.substr(slashPos + 1);
    year = atoi(date.c_str());
    if(year < 1)
        year = 1;
}

std::string Date::toString()
{
    if(day == INF)
        return "NOW";
    std::string result;
    std::stringstream stream;
    stream << day;
    result += stream.str() + '/';
    stream.str("");
    stream << month;
    result += stream.str() + '/';
    stream.str("");
    stream << year;
    result += stream.str();
    return result;
}

bool Date::operator < (const Date& date) const
{
    if(year != date.year)
        return year < date.year;
    if(month != date.month)
        return month < date.month;
    return day < date.day;
}

bool Date::operator != (const Date& date) const
{
    return (day != date.day) || (month != date.month) || (year != date.year);
}
