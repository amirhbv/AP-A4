#include "User.hpp"

User::User(std::string firstName_, std::string lastName_, std::string emailAddress_, std::string biography_)
{
    firstName = firstName_;
    lastName = lastName_;
    emailAddress = emailAddress_;
    biography = biography_;
}

bool User::addExperience(Experience experience)
{
    std::vector<Experience>::iterator experienceIter = std::find_if(experiences.begin(), experiences.end(), Experience::Compare(experience));
    if(experienceIter != experiences.end())
        return 0;
    experiences.push_back(experience);
    return 1;
}

void User::assignSkill(Skill skill)
{
    std::vector<Skill>::iterator skillIter = std::find_if(skills.begin(), skills.end(), Skill::Compare(skill.getName()));
    if(skillIter != skills.end())
        return;
    skills.push_back(skill);
}

void User::follow(User& user)
{
    for(std::vector<User*>::iterator userIter = friends.begin(); userIter != friends.end(); userIter++)
        if((*userIter)->getId() == user.getId())
            return;
    friends.push_back(&user);
}

bool User::isSkillRateEnough(Skill skill)
{
    std::vector<Skill>::iterator skillIter = std::find_if(skills.begin(), skills.end(), Skill::Compare(skill.getName()));
    if(skillIter == skills.end())
        return 0;
    if(skillIter->getRate() < skill.getRate())
        return 0;
    return 1;
}

float User::calculateSkillsRate(std::vector <Skill> conditions)
{
    float result = 0;

    for(unsigned int i = 0; i < conditions.size(); i++)
    {
        std::vector<Skill>::iterator skillIter = std::find_if(skills.begin(), skills.end(), Skill::Compare(conditions[i].getName()));
        if(skillIter != skills.end())
            result += skillIter->getRate();
    }
    return result;
}

int User::getEndorsersCount(std::string skillName)
{
    std::vector<Skill>::iterator skillIter = std::find_if(skills.begin(), skills.end(), Skill::Compare(skillName));
    if(skillIter == skills.end())
        return 0;
    return skillIter->getEndorsersCount();
}

void User::endorseSkill(std::string skillName, User endorserUser)
{
    std::vector<Skill>::iterator skillIter = std::find_if(skills.begin(), skills.end(), Skill::Compare(skillName));
    if(skillIter == skills.end())
        return;
    skillIter->endorseByUser(endorserUser.getId(), endorserUser.getEndorsersCount(skillName));
}

void User::printProfile()
{
    std::cout << "Name: " << firstName << ' ' << lastName << std::endl;
    std::cout << "Email: " << emailAddress << std::endl;
    std::cout << "Biography: " << biography << std::endl;
    std::cout << "Network: " << friends.size() << " connections" << std::endl;
    std::cout << "Experiences:" << std::endl;
    sort(experiences.begin(), experiences.end());
    int counter = 1;
    for(std::vector<Experience>::iterator experienceIter = experiences.begin(); experienceIter != experiences.end(); experienceIter++)
    {
        std::cout << '\t' << counter++ << ". ";
        experienceIter->print();
        std::cout << std::endl;
    }
    std::cout << "Skills:" << std::endl;
    std::cout.precision(2);
    std::cout.setf(std::ios::fixed, std::ios::floatfield);
    counter = 1;
    for(std::vector<Skill>::iterator skillIter = skills.begin(); skillIter != skills.end(); skillIter++)
    {
        std::cout << '\t' << counter++ << ". ";
        std::cout << skillIter->getName() << " - " << skillIter->getRate();
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void User::printNetwork(int level)
{
    std::vector<std::string> visitedUsersId;
    std::queue<User*> BFSQueue;
    for(std::vector<User*>::iterator friendIter = friends.begin(); friendIter != friends.end(); friendIter++)
        BFSQueue.push(*friendIter);
    BFSQueue.push(this);
    int counter = 1;
    while(!BFSQueue.empty() && level)
    {
        if(std::find(visitedUsersId.begin(), visitedUsersId.end(), BFSQueue.front()->getId()) != visitedUsersId.end())
        {
            BFSQueue.pop();
            continue;
        }
        if(BFSQueue.front()->getId() == this->getId())
        {
            BFSQueue.pop();
            BFSQueue.push(this);
            level--;
            continue;
        }
        if(level == 1)
        {
            std::cout << counter++ << ".\n";
            BFSQueue.front()->printProfile();
        }
        visitedUsersId.push_back(BFSQueue.front()->getId());
        for(std::vector<User*>::iterator friendIter = BFSQueue.front()->friends.begin(); friendIter != BFSQueue.front()->friends.end(); friendIter++)
            if((*friendIter)->getId() != this->getId())
                BFSQueue.push(*friendIter);
        BFSQueue.pop();
    }
}
