#include "Experience.hpp"

Experience::Experience(std::string jobTitle_, std::string companyName_, std::string startsAt, std::string endsAt)
    :startDate(startsAt), endDate(endsAt)
{
    jobTitle = jobTitle_;
    companyName = companyName_;
}

void Experience::print()
{
    std::cout << startDate.toString() << " - " << endDate.toString() << ' ' << jobTitle << " at " << companyName;
}

bool Experience::operator < (const Experience& experience) const
{
    if(startDate != experience.startDate)
        return startDate < experience.startDate;
    return endDate < experience.endDate;
}
