#include "LinkedIn.hpp"

void LinkedIn::addUser(std::string firstName, std::string lastName, std::string emailAddress, std::string biography)
{
    std::vector<User>::iterator userIter = std::find_if(users.begin(), users.end(), User::Compare(emailAddress));
    if(userIter != users.end())
        return;
    users.push_back(User(firstName, lastName, emailAddress, biography));
}

void LinkedIn::addCompany(std::string name, std::string address, std::string description)
{
    std::vector<Company>::iterator companyIter = std::find_if(companies.begin(), companies.end(), Company::Compare(name));
    if(companyIter != companies.end())
        return;
    companies.push_back(Company(name, address, description));
}

void LinkedIn::addExperience(std::string userId, std::string companyId, std::string title, std::string startsAt , std::string endsAt)
{
    if(Date(endsAt) < Date(startsAt))
        return;
    std::vector<User>::iterator userIter = std::find_if(users.begin(), users.end(), User::Compare(userId));
    if(userIter == users.end())
        return;
    std::vector<Company>::iterator companyIter = std::find_if(companies.begin(), companies.end(), Company::Compare(companyId));
    if(companyIter == companies.end())
        return;
    if(userIter->addExperience(Experience(title, companyId, startsAt, endsAt)))
        companyIter->addJob(Job(startsAt, endsAt, title, userIter->getFirstName(), userIter->getLastName()));
}

void LinkedIn::addJobRequest(std::string companyName, std::string title, std::map<std::string, float> conditions)
{
    std::vector<Company>::iterator companyIter = std::find_if(companies.begin(), companies.end(), Company::Compare(companyName));
    if(companyIter == companies.end())
        return;
    companyIter->addJobRequest(JobRequest(title, conditions));
}

void LinkedIn::assignSkill(std::string userId, std::string skillName)
{
    std::vector<User>::iterator userIter = std::find_if(users.begin(), users.end(), User::Compare(userId));
    if(userIter == users.end())
        return;
    userIter->assignSkill(Skill(skillName));
}

void LinkedIn::endorseSkill(std::string endorserUserId, std::string skilledUserId, std::string skillName)
{
    std::vector<User>::iterator endorserUserIter = std::find_if(users.begin(), users.end(), User::Compare(endorserUserId));
    if(endorserUserIter == users.end())
        return;
    std::vector<User>::iterator skilledUserIter = std::find_if(users.begin(), users.end(), User::Compare(skilledUserId));
    if(skilledUserIter == users.end())
        return;
    skilledUserIter->endorseSkill(skillName, *endorserUserIter);
}

void LinkedIn::follow(std::string followerId, std::string followingId)
{
    std::vector<User>::iterator followerUserIter = std::find_if(users.begin(), users.end(), User::Compare(followerId));
    if(followerUserIter == users.end())
        return;
    std::vector<User>::iterator followingUserIter = std::find_if(users.begin(), users.end(), User::Compare(followingId));
    if(followingUserIter == users.end())
        return;
    followerUserIter->follow(*followingUserIter);
    followingUserIter->follow(*followerUserIter);
}

void LinkedIn::applyForJob(std::string userId, std::string companyId, std::string jobTitle)
{
    std::vector<User>::iterator userIter = std::find_if(users.begin(), users.end(), User::Compare(userId));
    if(userIter == users.end())
        return;
    std::vector<Company>::iterator companyIter = std::find_if(companies.begin(), companies.end(), Company::Compare(companyId));
    if(companyIter == companies.end())
        return;
    companyIter->applyForJob(*userIter, jobTitle);
}

void LinkedIn::hireBestApplicant(std::string companyId, std::string jobTitle, std::string startsAt)
{
    std::vector<Company>::iterator companyIter = std::find_if(companies.begin(), companies.end(), Company::Compare(companyId));
    if(companyIter == companies.end())
        return;
    std::string hiredUserId = companyIter->hireBestApplicant(jobTitle);
    std::vector<User>::iterator userIter = std::find_if(users.begin(), users.end(), User::Compare(hiredUserId));
    if(userIter == users.end())
        return;
    addExperience(userIter->getId(), companyIter->getId(), jobTitle, startsAt);
}

void LinkedIn::printUserProfile(std::string userId)
{
    std::vector<User>::iterator userIter = std::find_if(users.begin(), users.end(), User::Compare(userId));
    if(userIter == users.end())
        return;
    userIter->printProfile();
}

void LinkedIn::printCompanyProfile(std::string companyName)
{
    std::vector<Company>::iterator companyIter = std::find_if(companies.begin(), companies.end(), Company::Compare(companyName));
    if(companyIter == companies.end())
        return;
    companyIter->printProfile();
}

void LinkedIn::printSuggestedJobs(std::string userId)
{
    std::vector<User>::iterator userIter = std::find_if(users.begin(), users.end(), User::Compare(userId));
    if(userIter == users.end())
        return;
    int counter = 1;
    for(std::vector<Company>::iterator companyIter = companies.begin(); companyIter != companies.end(); companyIter++)
    {
        std::vector<JobRequest> suggestedRequests = companyIter->suggestedRequests(*userIter);
        for(std::vector<JobRequest>::iterator requestIter = suggestedRequests.begin(); requestIter != suggestedRequests.end(); requestIter++)
        {
            std::cout << counter++ << ". ";
            requestIter->print(companyIter->getId());
            std::cout << std::endl;
        }
    }
}

void LinkedIn::printSuggestedUsers(std::string companyName, std::string jobTitle)
{
    std::vector<Company>::iterator companyIter = std::find_if(companies.begin(), companies.end(), Company::Compare(companyName));
    if(companyIter == companies.end())
        return;
    int counter = 1;
    for(std::vector<User>::iterator userIter = users.begin(); userIter != users.end(); userIter++)
    {
        if(companyIter->canUserApplyForJob(*userIter, jobTitle))
        {
            std::cout << counter++ << ".\n";
            userIter->printProfile();
        }
    }
}

void LinkedIn::printNetwork(std::string userId, int level)
{
    std::vector<User>::iterator userIter = std::find_if(users.begin(), users.end(), User::Compare(userId));
    if(userIter == users.end())
        return;
    userIter->printNetwork(level);
}
