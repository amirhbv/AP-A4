#include "Interface.hpp"

void Interface::addUser(std::string firstName, std::string lastName, std::string emailAddress, std::string biography)
{
    linkedIn.addUser(firstName, lastName, emailAddress, biography);
}

void Interface::addCompany(std::string name, std::string address, std::string description)
{
    linkedIn.addCompany(name, address, description);
}

void Interface::addExperience(std::string userId, std::string companyId, std::string title, std::string startAt , std::string endsAt)
{
    linkedIn.addExperience(userId, companyId, title, startAt, endsAt);
}

void Interface::addJobRequest(std::string companyName, std::string title, std::map<std::string, float> conditions)
{
    linkedIn.addJobRequest(companyName, title, conditions);
}

void Interface::assignSkill(std::string userId, std::string skillName)
{
    linkedIn.assignSkill(userId, skillName);
}

void Interface::endorseSkill(std::string endorserUserId, std::string skilledUserId, std::string skillName)
{
    linkedIn.endorseSkill(endorserUserId, skilledUserId, skillName);
}

void Interface::follow(std::string followerId, std::string followingId)
{
    linkedIn.follow(followerId, followingId);
}

void Interface::applyForJob(std::string userId, std::string companyId, std::string jobTitle)
{
    linkedIn.applyForJob(userId, companyId, jobTitle);
}

void Interface::hireBestApplicant(std::string companyId, std::string jobTitle, std::string startsAt)
{
    linkedIn.hireBestApplicant(companyId, jobTitle, startsAt);
}

void Interface::printUserProfile(std::string userId)
{
    linkedIn.printUserProfile(userId);
}

void Interface::printCompanyProfile(std::string companyName)
{
    linkedIn.printCompanyProfile(companyName);
}

void Interface::printSuggestedJobs(std::string userId)
{
    linkedIn.printSuggestedJobs(userId);
}

void Interface::printSuggestedUsers(std::string companyName, std::string jobTitle)
{
    linkedIn.printSuggestedUsers(companyName, jobTitle);
}

void Interface::printNetwork(std::string userId, int level)
{
    linkedIn.printNetwork(userId, level);
}
