#include "Company.hpp"

Company::Company(std::string name_, std::string address_, std::string description_)
{
    name = name_;
    address = address_;
    description = description_;
}

void Company::addJobRequest(JobRequest jobRequest)
{
    std::vector<JobRequest>::iterator requestIter = std::find_if(jobRequests.begin(), jobRequests.end(), JobRequest::Compare(jobRequest.getJobTitle()));
    if(requestIter != jobRequests.end())
        return;
    jobRequests.push_back(jobRequest);
}

void Company::applyForJob(User user, std::string jobTitle)
{
    std::vector<JobRequest>::iterator requestIter = std::find_if(jobRequests.begin(), jobRequests.end(), JobRequest::Compare(jobTitle));
    if(requestIter == jobRequests.end())
        return;
    requestIter->addApplicant(user);
}

std::string Company::hireBestApplicant(std::string jobTitle)
{
    std::vector<JobRequest>::iterator requestIter = std::find_if(jobRequests.begin(), jobRequests.end(), JobRequest::Compare(jobTitle));
    if(requestIter == jobRequests.end())
        return "";
    std::string bestApplicantId = requestIter->findBestApplicant();
    if(bestApplicantId.empty())
        return "";
    jobRequests.erase(requestIter);
    return bestApplicantId;
}

void Company::printProfile()
{
    std::cout << "Name: " << name << std::endl;
    std::cout << "Address: " << address << std::endl;
    std::cout << "Description: " << description << std::endl;
    std::cout << "Jobs:" << std::endl;
    sort(jobs.begin(), jobs.end());
    int counter = 1;
    for(std::vector<Job>::iterator jobIter = jobs.begin(); jobIter != jobs.end(); jobIter++)
    {
        std::cout << '\t' << counter++ << ". ";
        jobIter->print();
        std::cout << std::endl;
    }
    std::cout << "Requests:" << std::endl;
    counter = 1;
    for(std::vector<JobRequest>::iterator requestIter = jobRequests.begin(); requestIter != jobRequests.end(); requestIter++)
    {
        std::cout << '\t' << counter++ << ". ";
        requestIter->print();
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void Company::addJob(Job job)
{
    jobs.push_back(job);
}

std::vector<JobRequest> Company::suggestedRequests(User user)
{
    std::vector<JobRequest> result;
    for(std::vector<JobRequest>::iterator requestIter = jobRequests.begin(); requestIter != jobRequests.end(); requestIter++)
        if(requestIter->isUserOk(user))
            result.push_back(*requestIter);
    return result;
}

bool Company::canUserApplyForJob(User user, std::string jobTitle)
{
    std::vector<JobRequest>::iterator requestIter = std::find_if(jobRequests.begin(), jobRequests.end(), JobRequest::Compare(jobTitle));
    if(requestIter == jobRequests.end())
        return 0;
    return requestIter->isUserOk(user);
}
