#include "JobRequest.hpp"

JobRequest::JobRequest(std::string jobTitle_, std::map<std::string, float> conditions_)
{
    jobTitle = jobTitle_;
    std::map<std::string, float>::iterator iter = conditions_.begin();
    for(;iter != conditions_.end(); iter++)
        conditions.push_back(Skill(iter->first, iter->second));
}

void JobRequest::addApplicant(User user)
{
    if(isUserOk(user))
        applicants.push_back(user);
}

bool JobRequest::isUserOk(User user)
{
    for(std::vector<Skill>::iterator conditionIter = conditions.begin(); conditionIter != conditions.end(); conditionIter++)
        if(!user.isSkillRateEnough(*conditionIter))
            return 0;
    return 1;
}

std::string JobRequest::findBestApplicant()
{
    std::string result;
    float maximumSum = 0;
    for(std::vector<User>::iterator applicantIter = applicants.begin(); applicantIter != applicants.end(); applicantIter++)
    {
        float temp = applicantIter->calculateSkillsRate(conditions);
        if(temp > maximumSum)
        {
            maximumSum = temp;
            result = applicantIter->getId();
        }
    }
    return result;
}

void JobRequest::print(std::string companyName)
{
    std::cout << jobTitle;
    if(companyName != "")
        std::cout << " in " << companyName;
    std::cout << " - needed skills: ";
    std::cout.precision(2);
    std::cout.setf(std::ios::fixed, std::ios::floatfield);
    for(std::vector<Skill>::iterator conditionIter = conditions.begin(); conditionIter != conditions.end(); conditionIter++)
    {
        std::cout << ((conditionIter != conditions.begin())?", ":"");
        std::cout << conditionIter->getName() << "(" << conditionIter->getRate() << ")";
    }
}
