#ifndef JOBREQUEST_H
#define JOBREQUEST_H

#include <map>
#include <vector>
#include <iostream>
#include "User.hpp"
#include "Skill.hpp"

class JobRequest
{
    public:
        JobRequest(std::string jobTitle_, std::map<std::string, float> conditions_);
        class Compare{
            public:
                Compare(std::string input){ title = input; };
                bool operator()(const JobRequest& jobRequest)const{ return jobRequest.jobTitle == title; };
            private:
                std::string title;
        };
        std::string getJobTitle(){ return jobTitle; };
        void addApplicant(User user);
        bool isUserOk(User user);
        std::string findBestApplicant();
        void print(std::string companyName = std::string(""));
    private:
        std::string jobTitle;
        std::vector<Skill> conditions;
        std::vector<User> applicants;
};

#endif
