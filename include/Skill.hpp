#ifndef SKILL_H
#define SKILL_H

#include <cmath>
#include <vector>
#include <string>
#include <iostream>
#include <algorithm>

class Skill
{
    public:
        Skill(std::string name_, float rate_ = 0);
        class Compare{
            public:
                Compare(std::string input){ name = input; };
                bool operator()(const Skill& skill)const{ return skill.name == name; };
            private:
                std::string name;
        };
        std::string getName(){ return name; };
        float getRate(){ return rate; };
        unsigned int getEndorsersCount(){ return endorsersId.size(); };
        void endorseByUser(std::string endorserId, int endorserCount);
    private:
        std::string name;
        float rate;
        std::vector<std::string> endorsersId;
};

#endif
