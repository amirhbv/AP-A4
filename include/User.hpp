#ifndef USER_H
#define USER_H

#include <cmath>
#include <queue>
#include <vector>
#include <iostream>
#include <algorithm>
#include "Experience.hpp"
#include "Skill.hpp"

class User
{
    public:
        User(std::string firstName_, std::string lastName_, std::string emailAddress_, std::string biography_);
        class Compare{
            public:
                Compare(std::string input){ email = input; };
                bool operator()(const User& user)const{ return user.emailAddress == email; };
            private:
                std::string email;
        };
        std::string getId(){ return emailAddress; };
        std::string getFirstName(){ return firstName; };
        std::string getLastName(){ return lastName; };
        bool addExperience(Experience experience);
        void assignSkill(Skill skill);
        void follow(User& user);
        bool isSkillRateEnough(Skill skill);
        float calculateSkillsRate(std::vector <Skill> conditions);
        int getEndorsersCount(std::string skillName);
        void endorseSkill(std::string skillName, User endorserUser);
        void printProfile();
        void printNetwork(int level);
    private:
        std::string firstName;
        std::string lastName;
        std::string emailAddress;
        std::string biography;
        std::vector<Experience> experiences;
        std::vector<Skill> skills;
        std::vector<User*> friends;
};

#endif
