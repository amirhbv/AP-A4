#ifndef COMPANY_H
#define COMPANY_H

#include <vector>
#include <iostream>
#include <algorithm>
#include "Job.hpp"
#include "JobRequest.hpp"

class Company
{
    public:
        Company(std::string name_, std::string address_, std::string description_);
        class Compare{
            public:
                Compare(std::string input){ name = input; };
                bool operator()(const Company& company)const{ return company.name == name; };
            private:
                std::string name;
        };
        std::string getId(){ return name; };
        void addJobRequest(JobRequest jobRequest);
        void applyForJob(User user, std::string jobTitle);
        std::string hireBestApplicant(std::string jobTitle);
        void addJob(Job job);
        std::vector<JobRequest> suggestedRequests(User user);
        bool canUserApplyForJob(User user, std::string jobTitle);
        void printProfile();
    private:
        std::string name;
        std::string address;
        std::string description;
        std::vector<JobRequest> jobRequests;
        std::vector<Job> jobs;
};

#endif
