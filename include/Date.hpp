#ifndef DATE_H
#define DATE_H

#include <string>
#include <cstdlib>
#include <sstream>
#define NOW "INDIFINITE_DATE"
#define INF 9999

class Date
{
    public:
        Date(){};
        Date(std::string date);
        std::string toString();
        bool operator < (const Date& date) const;
        bool operator != (const Date& date) const;
    private:
        int strToInt(std::string str);
        std::string intToStr(int num);
        int day;
        int month;
        int year;
};

#endif
