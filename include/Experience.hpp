#ifndef EXPERIENCE_H
#define EXPERIENCE_H

#include <iostream>
#include "Date.hpp"

class Experience
{
    public:
        Experience(){};
        Experience(std::string jobTitle_, std::string companyName_, std::string startsAt, std::string endsAt);
        class Compare{
            public:
                Compare(Experience input_){ input = new Experience; *input = input_ ; };
                bool operator()(const Experience& experience)const
                {
                    if(experience.jobTitle != input->jobTitle || experience.companyName != input->companyName)
                        return 0;
                    return !((experience.endDate < input->startDate) || (input->endDate < experience.startDate));
                };
            private:
                Experience* input;
        };
        void print();
        bool operator < (const Experience& experience) const;
    private:
        std::string jobTitle;
        std::string companyName;
        Date startDate;
        Date endDate;
};

#endif
