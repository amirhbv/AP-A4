#ifndef JOB_H
#define JOB_H

#include <iostream>
#include "Date.hpp"

class Job
{
    public:
        Job(std::string startsAt, std::string endsAt, std::string jobTitle_, std::string userFirstName_, std::string userLastName_);
        void print();
        bool operator < (const Job& job) const;
    private:
        Date startDate;
        Date endDate;
        std::string jobTitle;
        std::string userFirstName;
        std::string userLastName;
};

#endif
