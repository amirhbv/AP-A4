#Makefile for AP-Assignment4
#4/6/2018

PROG = A4.out
CC = g++
CPPFLAGS = -Wall -pedantic -std=c++98 -Iinclude
OBJS = $(patsubst src/%.cpp, obj/%.o, $(wildcard src/*.cpp))

.PHONY: all clean build run debug

build: obj $(PROG)

run: build
	./$(PROG) $(RUN_ARGS)

debug : build
	$(CC) -g $(CPPFLAGS) main.cpp $(OBJS) -o $(PROG)
	gdb $(PROG)

all: clean run

$(PROG): main.cpp $(OBJS)
	$(CC) $(CPPFLAGS) main.cpp $(OBJS) -o $(PROG)

obj/Company.o: src/Company.cpp include/Company.hpp
	$(CC) $(CPPFLAGS) -c src/Company.cpp -o obj/Company.o

obj/Date.o: src/Date.cpp include/Date.hpp
	$(CC) $(CPPFLAGS) -c src/Date.cpp -o obj/Date.o

obj/Experience.o: src/Experience.cpp include/Experience.hpp
	$(CC) $(CPPFLAGS) -c src/Experience.cpp -o obj/Experience.o

obj/Interface.o: src/Interface.cpp include/Interface.hpp
	$(CC) $(CPPFLAGS) -c src/Interface.cpp -o obj/Interface.o

obj/Job.o: src/Job.cpp include/Job.hpp
	$(CC) $(CPPFLAGS) -c src/Job.cpp -o obj/Job.o

obj/JobRequest.o: src/JobRequest.cpp include/JobRequest.hpp
	$(CC) $(CPPFLAGS) -c src/JobRequest.cpp -o obj/JobRequest.o

obj/LinkedIn.o: src/LinkedIn.cpp include/LinkedIn.hpp
	$(CC) $(CPPFLAGS) -c src/LinkedIn.cpp -o obj/LinkedIn.o

obj/Skill.o: src/Skill.cpp include/Skill.hpp
	$(CC) $(CPPFLAGS) -c src/Skill.cpp -o obj/Skill.o

obj/User.o: src/User.cpp include/User.hpp
	$(CC) $(CPPFLAGS) -c src/User.cpp -o obj/User.o

obj:
	mkdir -p obj

clean:
	rm -f $(PROG)
	rm -r obj
