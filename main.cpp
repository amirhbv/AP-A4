#include <map>
#include <iostream>
#include "interface.hpp"

using namespace std;

int main()
{
    Interface interface;

    interface.printUserProfile("E1");
    cout << "A" << endl;
    interface.addUser("F1", "L1", "E1", "B1");
    interface.addUser("F2", "L2", "E2", "B2");
    interface.addUser("F3", "L3", "E3", "B3");
    interface.addUser("F4", "L4", "E4", "B4");
    interface.addUser("F5", "L5", "E5", "B5");
    interface.printUserProfile("E1");
    interface.addCompany("N1", "A1", "D1");
    interface.addCompany("N2", "A2", "D2");
    interface.addCompany("N3", "A3", "D3");
    interface.addExperience("E1", "N1", "J1", "1/7/1380", "1/4/1395");
    interface.addExperience("E1", "N1", "J2", "1/7/1395");
    interface.addExperience("E2", "N2", "J3", "1/4/1394", "30/3/1395");
    interface.addExperience("E2", "N1", "J4", "1/4/1395", "1/4/1396");
    interface.printUserProfile("E1");
    interface.assignSkill("E1", "S1");
    interface.assignSkill("E1", "S2");
    interface.assignSkill("E1", "S3");
    interface.assignSkill("E1", "S4");
    interface.assignSkill("E2", "S1");
    interface.assignSkill("E2", "S2");
    interface.assignSkill("E2", "S3");
    interface.assignSkill("E3", "S1");
    interface.assignSkill("E3", "S2");
    interface.assignSkill("E4", "S1");
    interface.printUserProfile("E1");

    interface.endorseSkill("E1", "E2", "S1");
    interface.endorseSkill("E3", "E2", "S1");
    interface.endorseSkill("E5", "E2", "S1");
    interface.endorseSkill("E2", "E3", "S1");
    interface.endorseSkill("E3", "E4", "S1");
    interface.endorseSkill("E4", "E1", "S1");
    interface.endorseSkill("E1", "E2", "S2");
    interface.endorseSkill("E3", "E2", "S2");
    interface.endorseSkill("E2", "E3", "S2");

    map<string, float> con1;
    con1["S1"] = 0;
    con1["S2"] = 0;
    interface.addJobRequest("N1", "J1", con1);
    map<string, float> con2;
    con2["S1"] = 1.5;
    interface.addJobRequest("N1", "J2", con2);
    map<string, float> con3;
    con3["S1"] = 1.5;
    con3["S4"] = 1.5;
    interface.addJobRequest("N1", "J3", con3);
    interface.addJobRequest("N1", "J3", con3);
    interface.addJobRequest("N2", "J3", con3);

    cout << "Print suggested jobs : " << endl;
    interface.printSuggestedJobs("E1");
    interface.printSuggestedJobs("E2");
    interface.printSuggestedJobs("E3");
    interface.printSuggestedJobs("E4");

    cout << "Print suggested users : " << endl;
    interface.printSuggestedUsers("N1", "J1");
    interface.printSuggestedUsers("N1", "J2");
    interface.printSuggestedUsers("N1", "J3");

    interface.printCompanyProfile("N1");

    interface.applyForJob("E1", "N1", "J1");
    interface.applyForJob("E2", "N1", "J1");
    interface.applyForJob("E3", "N1", "J1");
    interface.applyForJob("E4", "N1", "J1");
    interface.applyForJob("E1", "N1", "J2");
    interface.applyForJob("E2", "N1", "J2");
    interface.applyForJob("E3", "N1", "J2");
    interface.applyForJob("E4", "N1", "J2");
    interface.applyForJob("E1", "N1", "J3");
    interface.applyForJob("E2", "N1", "J3");
    interface.applyForJob("E3", "N1", "J3");
    interface.applyForJob("E4", "N1", "J3");

    interface.hireBestApplicant("N1", "J1", "1/7/1396");
    interface.hireBestApplicant("N1", "J3", "1/7/1397");
    interface.hireBestApplicant("N1", "J1", "1/7/1396");
    interface.printCompanyProfile("N1");
    interface.hireBestApplicant("N1", "J2", "1/7/1398");

    interface.follow("E1", "E2");
    interface.follow("E1", "E3");
    interface.follow("E1", "E4");
    interface.follow("E1", "E4");
    interface.follow("E2", "E4");
    interface.follow("E2", "E5");
    interface.follow("E3", "E5");
    interface.follow("E4", "E2");
    interface.follow("E4", "E1");

    cout << "Print User Profiles : " << endl;
    interface.printUserProfile("E1");
    interface.printUserProfile("E2");
    interface.printUserProfile("E3");
    interface.printUserProfile("E4");

    cout << "Print Company Profiles : " << endl;
    interface.printCompanyProfile("N1");
    interface.printCompanyProfile("N2");

    cout << "Print network : " << endl;
    cout << "Level 1 :" << endl;
    interface.printNetwork("E1", 1);
    cout << "Level 2 :" << endl;
    interface.printNetwork("E1", 2);
    cout << "Level 3 :" << endl;
    interface.printNetwork("E1", 3);

    return 0;
}



